package br.com.frederico_mesquita.behavioral.observer;

import br.com.frederico_mesquita.behavioral.observer.control.IOrderObserver;
import br.com.frederico_mesquita.behavioral.observer.control.Order;
import br.com.frederico_mesquita.behavioral.observer.control.OrderDispatcher;
import br.com.frederico_mesquita.behavioral.observer.control.PaymentObserver;
import br.com.frederico_mesquita.behavioral.observer.control.Waiter;
import br.com.frederico_mesquita.behavioral.observer.entity.Pizza;
import br.com.frederico_mesquita.behavioral.observer.entity.Sabor;
import br.com.frederico_mesquita.behavioral.observer.entity.Size;

public class Customer {

	public static void main(String[] args) {		
		Waiter waiter = Waiter.getInstance();
		
		IOrderObserver observer = PaymentObserver.getInstance();
		waiter.register(observer);
		observer = OrderDispatcher.getInstance();
		waiter.register(observer);
		
		waiter.takeOrder(Order.getInstance(Pizza.getInstance(Sabor.CALABRESA, Size.GIANT, 3)));
		waiter.takeOrder(Order.getInstance(Pizza.getInstance(Sabor.PORTUGUESA, Size.SMALL, 5)));

		waiter.placeOrder();
		
		
	}

}
