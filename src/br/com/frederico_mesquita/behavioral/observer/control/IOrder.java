package br.com.frederico_mesquita.behavioral.observer.control;

public interface IOrder {
	void execute();
}
