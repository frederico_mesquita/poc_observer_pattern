package br.com.frederico_mesquita.behavioral.observer.control;

import java.util.ArrayList;
import java.util.List;

public class Waiter {
	private static Waiter _instance = null;
	
	private List<IOrder> lstiOrder = new ArrayList<IOrder>();
	private List<IOrderObserver> lstIorderObserver = new ArrayList<IOrderObserver>();
	
	private Waiter() {}
	
	public static Waiter getInstance() {
		if(null == _instance) {
			_instance = new Waiter();
		}
		return _instance;
	}
	
	public void takeOrder(IOrder pIOrder) {
		this.lstiOrder.add(pIOrder);
	}
	
	public void placeOrder() {
		for(IOrder item : this.lstiOrder) {
			item.execute();
			notityAllObservers((Order) item);
		}
	}
	
	private void notityAllObservers(Order pOrder) {
		for(IOrderObserver item : lstIorderObserver) {
			item.update(pOrder);
		}
	}
	
	public void register(IOrderObserver iOrderObserver) {
		lstIorderObserver.add(iOrderObserver);
		iOrderObserver.setSubject(this);
	}
}
