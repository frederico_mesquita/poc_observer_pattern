package br.com.frederico_mesquita.behavioral.observer.control;

public class OrderDispatcher implements IOrderObserver {
	
	private OrderDispatcher() {}
	
	public static OrderDispatcher getInstance() {
		return new OrderDispatcher();
	}
	
	private Waiter waiter = null;

	@Override
	public void update(Order pOrder) {
		System.out.println("Dispatched: Qty: " + pOrder.getPizza().getQty() + " / Flavour: " + pOrder.getPizza().getSabor().toString() + " / Size: " + pOrder.getPizza().getSize().toString());
	}

	@Override
	public void setSubject(Waiter waiter) {
		this.waiter = waiter;
	}

}
