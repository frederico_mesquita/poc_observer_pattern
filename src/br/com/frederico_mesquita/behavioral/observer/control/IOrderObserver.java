package br.com.frederico_mesquita.behavioral.observer.control;

public interface IOrderObserver {
	void update(Order pOrder);
	void setSubject(Waiter waiter);
}
