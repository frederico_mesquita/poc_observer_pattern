package br.com.frederico_mesquita.behavioral.observer.control;

public class PaymentObserver implements IOrderObserver {
	
	private Waiter waiter = null;
	
	private PaymentObserver() {}
	
	public static PaymentObserver getInstance() {
		return new PaymentObserver();
	}

	@Override
	public void update(Order pOrder) {
		System.out.println("Payment is done to order: Qty: " + pOrder.getPizza().getQty() + " / Flavour: " + pOrder.getPizza().getSabor().toString() + " / Size: " + pOrder.getPizza().getSize().toString());
	}

	@Override
	public void setSubject(Waiter waiter) {
		this.waiter = waiter;
	}

}
