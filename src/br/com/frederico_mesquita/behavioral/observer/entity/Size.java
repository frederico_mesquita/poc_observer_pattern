package br.com.frederico_mesquita.behavioral.observer.entity;

public enum Size {
	SMALL, MEDIUM, BIG, GIANT;
}
