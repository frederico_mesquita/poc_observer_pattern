package br.com.frederico_mesquita.behavioral.observer.entity;

public enum Sabor {
	CALABRESA, PEPERONI, QUATRO_QUEIJOS, PORTUGUESA, NAPOLITANA;
}
