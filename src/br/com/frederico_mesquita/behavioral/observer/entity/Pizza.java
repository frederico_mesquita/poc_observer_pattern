package br.com.frederico_mesquita.behavioral.observer.entity;

public class Pizza {
	private static Pizza _instance = null;
	
	private Sabor sabor;
	private int qty;
	private Size size;
	
	private Pizza() {}
	
	private Pizza(Sabor pSabor, Size pSize, int pQty) {
		this.sabor = pSabor;
		this.size = pSize;
		this.qty = pQty;
	}
	
	public static Pizza getInstance(Sabor pSabor, Size pSize, int pQty) {
		_instance = new Pizza(pSabor, pSize, pQty);
		return _instance;
	}
	
	public void makeOrder() {
		System.out.println("Pedido: Quantidade: " + this.qty + " / Sabor: " + sabor.toString() + " / Tamanho: " + size.toString());
	}
	
	public void cancelOrder() {
		System.out.println("Pedido cancelado: Quantidade: " + this.qty + " / Sabor: " + sabor.toString() + " / Tamanho: " + size.toString());
	}

	public Sabor getSabor() {
		return sabor;
	}

	public void setSabor(Sabor sabor) {
		this.sabor = sabor;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public Size getSize() {
		return size;
	}

	public void setSize(Size size) {
		this.size = size;
	}
}
